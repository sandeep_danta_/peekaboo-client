var mapMarkers = [];
var mapCircles = [];

function initialize() {
  var mapOptions = {
    zoom: 15
  };
  var map = new google.maps.Map(document.getElementById('mapCanvas'), mapOptions);
  //Set the current detected location as the center
  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      console.log(pos);
      var infoWindow = new google.maps.InfoWindow({
        'map': map,
        'position': pos,
        'content': 'You are here!'
      });
      map.setCenter(pos);
    }, function() {
      handleNoGeoLocation(true);
    });
  } else {
    handleNoGeoLocation(false);
  }
  google.maps.event.addListener(map, 'click', function(e) {
    addCircle();
  });
}

function addMarker() {
  //Add a marker to the map while storing the reference in mapMarkers
  //Also sets the click event listener for the marker
  var markerOpts = {
    'map': map,
    'position': e.latLng,
    'draggable': true
  }

  var marker = new google.maps.Marker(markerOpts);
  mapMarkers.push(marker);
  google.maps.event.addListener(marker, 'click', function(e) {
    console.log(e);
  });
}

function addCircle() {
  //Add a circle to the map while storing the reference in mapCircles
  var circleOpts = {
    'map': map,
    'center': e.latLng,
    'radius': 500,
    'fillColor': '#217aa5',
    'fillOpacity': 0.2,
    'editable': true
  }
  var circle =  new google.maps.Circle(circleOpts);
  mapCircles.push(circle);
}

function clearAllMarkers() {
  //Remove all existing markers on the map
  for(var i in mapMarkers) {
    mapMarkers[i].setMap(null);
    mapMarkers[i] = null;
  }
}

function clearAllCircles() {
  //Remove all existing circles on the map
  for(var i in mapCircles) {
    mapCircles[i].setMap(null);
    mapCircles[i] = null;
  }
}

function handleNoGeolocation(errorFlag) {
  if (errorFlag) {
    var content = 'Error: The Geolocation service failed.';
  } else {
    var content = 'Error: Your browser doesn\'t support geolocation.';
  }

  var options = {
    map: map,
    position: new google.maps.LatLng(60, 105),
    content: content
  };

  var infoWindow = new google.maps.InfoWindow(options);
  map.setCenter(options.position);
}

google.maps.event.addDomListener(window, 'load', initialize);
